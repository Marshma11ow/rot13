#include <iostream>


class rot13 {

public:
static char encode(char input);

};

char rot13::encode(char input) {	

    //std::cout << "Searching for appropriate character." << std::endl;

    if (input == 'A') return 'N';
    if (input == 'B') return 'O';
    if (input == 'C') return 'P';
    if (input == 'D') return 'Q';
    if (input == 'E') return 'R';
    if (input == 'F') return 'S';
    if (input == 'G') return 'T';
    if (input == 'H') return 'U';
    if (input == 'I') return 'V';
    if (input == 'J') return 'W';
    if (input == 'K') return 'X';
    if (input == 'L') return 'Y';
    if (input == 'M') return 'Z';
    if (input == 'N') return 'A';
    if (input == 'O') return 'B';
    if (input == 'P') return 'C';
    if (input == 'Q') return 'D';
    if (input == 'R') return 'E';
    if (input == 'S') return 'F';
    if (input == 'T') return 'G';
    if (input == 'U') return 'H';
    if (input == 'V') return 'I';
    if (input == 'W') return 'J';
    if (input == 'X') return 'K';
    if (input == 'Y') return 'L';
    if (input == 'Z') return 'M';

    if (input == 'a') return 'n';
    if (input == 'b') return 'o';
    if (input == 'c') return 'p';
    if (input == 'd') return 'q';
    if (input == 'e') return 'r';
    if (input == 'f') return 's';
    if (input == 'g') return 't';
    if (input == 'h') return 'u';
    if (input == 'i') return 'v';
    if (input == 'j') return 'w';
    if (input == 'k') return 'x';
    if (input == 'l') return 'y';
    if (input == 'm') return 'z';
    if (input == 'n') return 'a';
    if (input == 'o') return 'b';
    if (input == 'p') return 'c';
    if (input == 'q') return 'd';
    if (input == 'r') return 'e';
    if (input == 's') return 'f';
    if (input == 't') return 'g';
    if (input == 'u') return 'h';
    if (input == 'v') return 'i';
    if (input == 'w') return 'j';
    if (input == 'x') return 'k';
    if (input == 'y') return 'l';
    if (input == 'z') return 'm';

    return input;

}








